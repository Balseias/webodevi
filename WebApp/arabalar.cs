namespace WebApp
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("arabalar")]
    public partial class arabalar
    {
        public arabalar()
        {
            satin_alinanlar = new HashSet<satin_alinanlar>();
        }
        [Key]
        public int id { get; set; }

        [Required(ErrorMessage=" ")]
        [StringLength(10)]
        public string plaka { get; set; }

        [Required(ErrorMessage = " ")]
        public int gunlukKiraBedeli { get; set; }

        public int markaid { get; set; }

        public int kiradaMi { get; set; }

        [Required(ErrorMessage = " ")]
        [StringLength(150)]
        public string arabaResmi { get; set; }

        [Required(ErrorMessage = " ")]
        [StringLength(150)]
        public string arabaAdi { get; set; }

        public virtual markalar markalar { get; set; }

        public virtual ICollection<satin_alinanlar> satin_alinanlar { get; set; }
    }
}
