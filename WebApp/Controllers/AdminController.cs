﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{

      //  [Authorize(Roles="Admin")]
    [UserAuthorize]
    public class AdminController : Controller
    {
        model db = new model();
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UyeListele()
        {
            return View(db.uyeler.ToList());
        }
        [HttpGet]
        public ActionResult UyeDuzenle(int? id)
        {
            if (id != null)
            {
                var uye = db.uyeler.Where(x => x.id == id).ToList();
                if (uye.Count !=0)
                    return View(uye.First());
                else
                    return RedirectToAction("UyeListele");
            }
            return RedirectToAction("UyeListele");
        }
        [HttpPost]
        public ActionResult UyeDuzenle(uyeler uye)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uye).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("UyeListele");
        }

        [HttpGet]
        public ActionResult ArabaDuzenle(int? id)
        {
            if (id != null)
            {
                var araba = db.arabalar.Where(x => x.id == id).ToList();
                if (araba.Count != 0)
                {
                    return View(Tuple.Create(araba.First(), db.markalar.ToList()));
                }
                else
                    return RedirectToAction("ArabalariListele");
            }
            return RedirectToAction("ArabalariListele");
        }

        [HttpPost]
        public ActionResult ArabaDuzenle([Bind(Prefix = "Item1")]arabalar araba)
        {
            if (arabaIsValid(araba))
            {
                db.Entry(araba).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("ArabalariListele");
        }
        public ActionResult MarkalariListele()
        {
            return View(db.markalar.ToList());
        }
        public ActionResult MarkaEkle()
        {
            return View();
        }
        [HttpPost]
        public ActionResult MarkaEkle(string marka)
        {
            if (!String.IsNullOrEmpty(marka))
            {
                var markaS = db.markalar.Where(x => x.marka == marka).ToList();
                if (markaS.Count !=0)
                    ModelState.AddModelError("", "Böyle bir marka daha önce girilmiş");
                else
                {
                    markalar _marka = new markalar();
                    _marka.marka = marka;
                    db.markalar.Add(_marka);
                    db.SaveChanges();
                    ModelState.AddModelError("", marka+" markanız eklenmiştir");

                }

            }
            else
             ModelState.AddModelError("", "Boş veya Geçersiz girdiniz");
            return View();
        }
        public ActionResult ArabaEkle()
         {
             var araba = new arabalar();
             var marka = db.markalar.ToList();
             return View(Tuple.Create(araba,marka));
         }
        [HttpPost]
        public ActionResult ArabaEkle(arabalar car)
        {
            if (arabaIsValid(car) && !kayitVarMi(car))
            {
                car.kiradaMi = 0;
                db.arabalar.Add(car);
                db.SaveChanges();
                ModelState.AddModelError("", "Arabanız eklenmiştir");
            }
            var araba = new arabalar();
            var marka = db.markalar.ToList();
            return View(Tuple.Create(araba, marka));
        }

        public ActionResult ArabalariListele()
        {
            return View(db.arabalar.ToList());
        }
        [HttpGet]
        public ActionResult MarkaDuzenle(int? id)
        {
            if (id != null)
            {
                var marka = db.markalar.Where(x => x.id == id).ToList();
                if (marka.Count != 0)
                {
                    return View(marka.First());
                }
                else
                    return RedirectToAction("MarkalariListele");
            }
            return RedirectToAction("MarkalariListele");
        }
        [HttpPost]
        public ActionResult MarkaDuzenle(int id,string markaAdi)
        {
            if (!String.IsNullOrEmpty(markaAdi))
            {
                markalar m = db.markalar.Where(x => x.id == id).First();
                m.marka = markaAdi;
                db.Entry(m).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("MarkalariListele");
            }
            else
                ModelState.AddModelError("", "Boş veya geçersiz");
            return Redirect(HttpContext.Request.UrlReferrer.ToString());

        }
        [HttpGet]
        public ActionResult MarkaSil(int? id)
        {
            if (id != null)
            {
                var marka = db.markalar.Where(x => x.id == id).ToList();
                if (marka.Count != 0)
                {
                    return View(marka.First());
                }
                else
                    return RedirectToAction("MarkalariListele");
            }
            return RedirectToAction("MarkalariListele");
        }
        [HttpGet]
        public ActionResult ArabaSil(int? id)
        {
            if (id != null)
            {
                var araba = db.arabalar.Where(x => x.id == id).ToList();
                if (araba.Count != 0)
                {
                    return View(araba.First());
                }
                else
                    return RedirectToAction("ArabalariListele");
            }
            return RedirectToAction("ArabalariListele");
        }
        [HttpPost]
        public ActionResult ArabaSil(arabalar araba)
        {
            // Sadece kirada olmayan arabalar silinebilir
            if (araba.kiradaMi==0)
            {
                arabalar gercekAraba = db.arabalar.Where(x => x.id == araba.id).First();
                db.arabalar.Remove(gercekAraba);
                db.SaveChanges();
            }
            return RedirectToAction("ArabalariListele");
        }
        [HttpGet]
        public ActionResult UyeSil(int? id)
        {
            if (id != null)
            {
                var uye = db.uyeler.Where(x => x.id == id).ToList();
                if (uye.Count != 0)
                {
                    return View(uye.First());
                }
                else
                    return RedirectToAction("UyeListele");
            }
            return RedirectToAction("UyeListele");
        }
        [HttpPost]
        public ActionResult UyeSil(uyeler uye)
        {
            // Sadece normal kullanıcılar silinebilir
            if (uye.uyeTip == 0)
            {
                uyeler gercekUye = db.uyeler.Where(x => x.id == uye.id).First();
                db.uyeler.Remove(gercekUye);
                db.SaveChanges();
            }
            return RedirectToAction("UyeListele");
        }
        [HttpPost]
        public ActionResult MarkaSil(int id,string marka)
        {
            if (!arabaKayitliMi(id))
            {
                markalar markaKayit = db.markalar.Where(x => x.id == id).First();
                db.markalar.Remove(markaKayit);
                db.SaveChanges();
                return RedirectToAction("MarkalariListele");
            }
            else
            {
                ModelState.AddModelError("", "Bu markaya kayıtlı olan araçlar var onları değiştirin");
                return View(db.markalar.Where(x => x.id == id).First());
            }
        }
        public ActionResult GetirilenArabalar()
        {
            return View(db.satin_alinanlar.Where(x=>x.geriGetirdiMi == 0).ToList());
        }
        [HttpGet]
        public ActionResult ArabaGetirildi(int? id)
        {
            if (id!=null)
            {
                if (satinAlKayitVarMi(id.Value))
                {
                    return View(db.satin_alinanlar.Where(x => x.id == id).First());
                }
            }
            return RedirectToAction("GetirilenArabalar", "Admin");
        }
        [HttpPost]
        public ActionResult ArabaGetirildi(satin_alinanlar satinAl)
        {
            satin_alinanlar satinAlAlter = db.satin_alinanlar.Where(x => x.id == satinAl.id).First();
            satinAlAlter.geriGetirdiMi = 1;
            db.Entry(satinAlAlter).State = EntityState.Modified;

            arabalar arabaAlter = db.arabalar.Where(x => x.id == satinAlAlter.arabaid).First();
            arabaAlter.kiradaMi = 0;
            db.Entry(arabaAlter).State = EntityState.Modified;

            db.SaveChanges();

            return RedirectToAction("GetirilenArabalar", "Admin");
        }

        bool satinAlKayitVarMi(int id)
        {
            var kayit = db.satin_alinanlar.Where(x => x.id == id).ToList();
            if (kayit.Count > 0)
            {
                return true;
            }
            return false;

        }
        private bool arabaKayitliMi(int id)
        {
            var arabaSayisi = db.arabalar.Where(x => x.markaid == id).ToList();
            if (arabaSayisi.Count==0)
            {
                return false;
            }
            return true;
        }
        private bool kayitVarMi(arabalar car)
        {
            var arabalar = db.arabalar.Where(x => x.arabaAdi == car.arabaAdi && x.plaka == car.plaka).ToList();
            if (arabalar.Count > 0)
            {
                ModelState.AddModelError("", "Bu plakada veya isimde zaten bir kayıt var");
                return true;
            }
            return false;
           
        }

        bool arabaIsValid(arabalar araba)
        {
            bool gonder = true;
            if(String.IsNullOrEmpty(araba.arabaAdi))
            {
                ModelState.AddModelError("", "Araba adı boş veya geçersiz girilmiş");
                gonder = false;
            }
            if(String.IsNullOrEmpty(araba.plaka))
            {
                ModelState.AddModelError("", "Plaka boş veya geçersiz girilmiş");
                gonder = false;
            }
            if (String.IsNullOrEmpty(araba.arabaResmi))
            {
                ModelState.AddModelError("", "Araba Resmi boş veya geçersiz girilmiş");
                gonder = false;
            }
            else
            {
                if (!araba.arabaResmi.Contains('.'))
                {
                    ModelState.AddModelError("", "Araba Resmi uzantısız olamaz");
                    gonder = false;
                }
                else
                {
                    if (araba.arabaResmi.Count(x=> x== '.') != 1 )
                    {
                        ModelState.AddModelError("", "Araba Resminde sadece 1 tane nokta olabilir");
                        gonder = false;
                    }
                    else
                    {
                        string[] uzantilar = { "jpeg","jpg","png" };
                        string resim = araba.arabaResmi;
                        resim = resim.Substring(resim.IndexOf('.')+1);
                        if (!uzantilar.Contains(resim))
                        {
                            ModelState.AddModelError("", "Uzantı uyuşmazlığı"+resim);
                            gonder = false;
                        }
                    }
                }
            }
            try
            {
                if (araba.gunlukKiraBedeli<=0)
                {
                    ModelState.AddModelError("", "Günlük kira bedeli negatif veya sıfır (0) olamaz");
                    gonder = false;
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
                gonder = false;
            }
            return gonder;
        }
        
    }
}