﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        model db = new model();
        public ActionResult Index()
        {
            return View(db.arabalar.Where(x=>x.kiradaMi==0).ToList());
        }

        public ActionResult About()
        {

            return View();
        }

        public ActionResult Contact()
        {

            return View();
        }
        public ActionResult UyeOl()
        {
            if (Request.Cookies.Get("kullaniciAdi")!=null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        [HttpPost]
        public ActionResult UyeOl(uyeler uye,string userPass1)
        {
            if (ModelState.IsValid)
            {
                if (!uyeKayitliMi(uye.kullaniciAdi,uye.eMail) && sifre(uye.sifre,userPass1))
                {
                    uye.uyeTip = 0;
                    db.uyeler.Add(uye);
                    db.SaveChanges();
                    return RedirectToAction("GirisYap");
                }
            }
            return View();
        }
        public ActionResult GirisYap()
        {
            if (Request.Cookies.Get("kullaniciAdi") != null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        [HttpPost]
        public ActionResult GirisYap(string userName,string Password)
        {
            var uye = db.uyeler.Where(x => x.kullaniciAdi == userName && x.sifre == Password).ToList();
            if(uye.Count > 0)
            {
                var userCookie = new HttpCookie("kullaniciAdi",uye[0].kullaniciAdi);
                var typeCookie = new HttpCookie("tip", uye[0].uyeTip.ToString());
                Response.SetCookie(userCookie);
                Response.SetCookie(typeCookie);
                /*if(uye.uyeTip==1)
                {
                    FormsAuthentication.SetAuthCookie(uye.kullaniciAdi,false);
                    //Roles.AddUserToRole(userName,"Admin");
                }*/
                return RedirectToAction("Index");
            }
            return View();
        }
        public ActionResult CikisYap()
        {
            var userCookie = Request.Cookies["kullaniciAdi"];
            var typeCookie = Request.Cookies["tip"];

            userCookie.Expires = DateTime.Now.AddDays(-1);
            typeCookie.Expires = DateTime.Now.AddDays(-1);

            Response.Cookies.Add(userCookie);
            Response.Cookies.Add(typeCookie);
            /*if(typeCookie.Value == "1")
            {
                FormsAuthentication.SignOut();
            }*/
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult ArabaSatinAl(int? id)
        {
            if (id != null)
            {
                if (arabaVarMi(id))
                {
                    if (!arabaKiradaMi(id))
                    {
                        if (Request.Cookies.Get("kullaniciAdi") != null)
                        {
                            var araba = db.arabalar.Where(x => x.id == id).First();
                            araba.kiradaMi = 1;
                            db.Entry(araba).State = EntityState.Modified;

                            satin_alinanlar satinAl = new satin_alinanlar();
                            satinAl.arabaid = id.Value;
                            var cookie = Request.Cookies.Get("kullaniciAdi");
                            satinAl.uyeid = db.uyeler.Where(x => x.kullaniciAdi == cookie.Value.ToString()).First().id;
                            satinAl.geriGetirdiMi = 0;
                            satinAl.alinmaTarihi = DateTime.Now.Date;
                            db.satin_alinanlar.Add(satinAl);

                            db.SaveChanges();
                            ModelState.AddModelError("", "Araba satın alımıştır");
                        }
                        else
                            ModelState.AddModelError("", "Araba Kiralamak için önce giriş yapmalısınız");
                    }
                    else
                        ModelState.AddModelError("", "Bu araba zaten kirada!");
                }
                else
                    ModelState.AddModelError("", "Böyle bir araba yok!");
            }
            else
                return RedirectToAction("Index", "Home");
            return View();
        }
        public ActionResult DilDegistir(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }
        bool arabaKiradaMi(int? id)
        {
            var k = db.arabalar.Where(x => x.id == id).First();
            if (k.kiradaMi==0)
            {
                return false;
            }
            return true;
        }
        bool arabaVarMi(int? id)
        {
            var k = db.arabalar.Where(x => x.id == id).ToList();
            if (k.Count==0)
            {
                return false;
            }
            return true;
        }
        bool uyeKayitliMi(string kullaniciAdi,string eMail)
        {
            bool kayit = false;
            var kKayit = db.uyeler.Where(x => x.kullaniciAdi == kullaniciAdi).ToList();
            if (kKayit.Count > 0)
            {
                kayit = true;
                ModelState.AddModelError("", "Bu kullanıcı adında zaten kayıt var!");
            }
            kKayit = db.uyeler.Where(x => x.eMail == eMail).ToList();
            if (kKayit.Count > 0)
            {
                kayit = true;
                ModelState.AddModelError("", "Bu E mailde zaten kayıtlı bir kullanıcı var");
            }

            return kayit;
        }
        bool sifre(string sifre1,string sifre2)
        {
            bool dogru = true;
            if (sifre1!=sifre2)
            {
                dogru = false;
                ModelState.AddModelError("", "Şifreler uyuşmuyor");

            }
            else
            {
                if (sifre1.Length<5)
                {
                    dogru = false;
                    ModelState.AddModelError("", "Şifre uzunluğu minimum 5 karakterdir");
                }
            }
            return dogru;

        }

    }
}