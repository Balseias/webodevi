﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class UserAuthorize : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext.Request.Cookies["tip"] != null && httpContext.Request.Cookies.Get("tip").Value == "1")
            {
                    return true;
                
            }
            else
            {
                httpContext.Response.Redirect("/");
                return false;
            }

        }
    }
}