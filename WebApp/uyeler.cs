namespace WebApp
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("uyeler")]
    public partial class uyeler
    {
        public uyeler()
        {
            satin_alinanlar = new HashSet<satin_alinanlar>();
        }
        [Key]
        public int id { get; set; }

        [Required(ErrorMessage="Hatal� Kullan�c� Ad�")]
        [StringLength(15)]
        public string kullaniciAdi { get; set; }

        [Required(ErrorMessage="Hatal� EMail")]
        [StringLength(50)]
        public string eMail { get; set; }

        [Required(ErrorMessage="Hatal� �ifre")]
        [StringLength(32)]
        public string sifre { get; set; }
        public int uyeTip { get; set; }

        public virtual ICollection<satin_alinanlar> satin_alinanlar { get; set; }
    }
}
