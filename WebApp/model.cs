namespace WebApp
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class model : DbContext
    {
        public model()
            : base("name=model")
        {
        }

        public virtual DbSet<arabalar> arabalar { get; set; }
        public virtual DbSet<markalar> markalar { get; set; }
        public virtual DbSet<satin_alinanlar> satin_alinanlar { get; set; }
        public virtual DbSet<uyeler> uyeler { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<arabalar>()
                .Property(e => e.plaka)
                .IsFixedLength();

            modelBuilder.Entity<arabalar>()
                .Property(e => e.gunlukKiraBedeli);

            modelBuilder.Entity<arabalar>()
                .Property(e => e.arabaResmi)
                .IsFixedLength();

            modelBuilder.Entity<arabalar>()
                .HasMany(e => e.satin_alinanlar)
                .WithRequired(e => e.arabalar)
                .HasForeignKey(e => e.arabaid)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<markalar>()
                .Property(e => e.marka)
                .IsFixedLength();

            modelBuilder.Entity<markalar>()
                .HasMany(e => e.arabalar)
                .WithRequired(e => e.markalar)
                .HasForeignKey(e => e.markaid)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<uyeler>()
                .Property(e => e.kullaniciAdi)
                .IsFixedLength();

            modelBuilder.Entity<uyeler>()
                .Property(e => e.eMail)
                .IsFixedLength();

            modelBuilder.Entity<uyeler>()
                .Property(e => e.sifre)
                .IsFixedLength();

            modelBuilder.Entity<uyeler>()
                .HasMany(e => e.satin_alinanlar)
                .WithRequired(e => e.uyeler)
                .HasForeignKey(e => e.uyeid)
                .WillCascadeOnDelete(false);
        }
    }
}
