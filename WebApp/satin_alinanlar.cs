namespace WebApp
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class satin_alinanlar
    {
        [Key]
        public int id { get; set; }

        public int uyeid { get; set; }

        public int arabaid { get; set; }

        public int? geriGetirdiMi { get; set; }
        public DateTime alinmaTarihi { get; set; }

        public virtual arabalar arabalar { get; set; }

        public virtual uyeler uyeler { get; set; }
    }
}
