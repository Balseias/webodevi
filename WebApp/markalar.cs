namespace WebApp
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("markalar")]
    public partial class markalar
    {
        public markalar()
        {
            arabalar = new HashSet<arabalar>();
        }
        [Key]
        public int id { get; set; }

        [Required(ErrorMessage="Bo� giremezsiniz")]
        [StringLength(50)]
        public string marka { get; set; }

        public virtual ICollection<arabalar> arabalar { get; set; }
    }
}
